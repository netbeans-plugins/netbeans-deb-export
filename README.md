# netbeans-deb-export

NetBeans plug-in for exporting a Debian installer package of a Java application project.

#### [Download binary releases here](https://gitlab.com/netbeans-plugins/netbeans-deb-export/tags/Release)

![Selection_022](/uploads/f7be9fcf82198b868661f3adb152a9da/Selection_022.png)
![Selection_034](/uploads/c148b5482321b6d906b3b1edb39e194e/Selection_034.png)