/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.export.deb;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;
import javax.swing.JDialog;
import org.netbeans.api.project.Project;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle.Messages;

@ActionID(
        category = "Project",
        id = "ide.export.deb.ExportDebAction"
)
@ActionRegistration(
        iconBase = "ide/export/deb/deb.png",
        displayName = "#CTL_ExportDebAction",
        menuText = "To DEB",
        iconInMenu = false
)
@ActionReferences({
    @ActionReference(path = "Menu/File/Export", position = 0, name = "To DEB"),
    @ActionReference(path = "Toolbars/Build", position = 500, name = "Export Project to Debian Package")
})
@Messages("CTL_ExportDebAction=Export Project to Debian Package")
public final class ExportDebAction implements ActionListener {

    private final Project context;
    private String project_path;
    private String project_properties_path;
    private String project_private_properties_path;
    private String deb_properties_path;
    private String platform_path;
    private String user_root_path;
    private String ant_runtime_path;
    private String build_file_path;

    public ExportDebAction(Project context) {
        this.context = context;
        platform_path = System.getProperty("netbeans.home");
        user_root_path = System.getProperty("netbeans.default_userdir_root");
        project_path = this.context.getProjectDirectory().getPath();
        project_properties_path = project_path + System.getProperty("file.separator") + "nbproject" + System.getProperty("file.separator") + "project.properties";
        project_private_properties_path = project_path + System.getProperty("file.separator") + "nbproject" + System.getProperty("file.separator") + "private" + System.getProperty("file.separator") + "private.properties";
        deb_properties_path = project_path + System.getProperty("file.separator") + "nbproject" + System.getProperty("file.separator") + "deb.properties";
        ant_runtime_path = platform_path.substring(0, platform_path.length() - 8) + "extide" + System.getProperty("file.separator") + "ant" + System.getProperty("file.separator") + "bin" + System.getProperty("file.separator") + "ant";
        build_file_path = project_path + System.getProperty("file.separator") + "nbproject" + System.getProperty("file.separator") + "build-impl.xml";

    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        try {
            Properties properties_file_data = new Properties();
            properties_file_data.load(new DataInputStream(new FileInputStream(project_properties_path)));

            Properties private_properties_file_data = new Properties();
            private_properties_file_data.load(new DataInputStream(new FileInputStream(project_private_properties_path)));

            Properties platform_properties = new Properties();
            platform_properties.setProperty("platform.path", platform_path);
            platform_properties.setProperty("deb.properties.path", deb_properties_path);
            platform_properties.setProperty("ant.runtime.path", ant_runtime_path);
            platform_properties.setProperty("build.file.path", build_file_path);
            platform_properties.setProperty("project.dir", project_path);
            platform_properties.setProperty("dist.dir", project_path + System.getProperty("file.separator") + properties_file_data.getProperty("dist.dir"));
            platform_properties.setProperty("dist.jar", properties_file_data.getProperty("dist.jar").replace("${dist.dir}", platform_properties.getProperty("dist.dir")));
            
            if(properties_file_data.containsKey("application.desc")){
                platform_properties.setProperty("application.desc", properties_file_data.getProperty("application.desc"));
            }else{
                platform_properties.setProperty("application.desc", "");
            }
            
            if (properties_file_data.containsKey("run.jvmargs")) {
                platform_properties.setProperty("run.jvmargs", properties_file_data.getProperty("run.jvmargs"));
            } else {
                platform_properties.setProperty("run.jvmargs", "");
            }
            if (private_properties_file_data.containsKey("application.args")) {
                platform_properties.setProperty("application.args", private_properties_file_data.getProperty("application.args"));
            } else {
                platform_properties.setProperty("application.args", "");
            }
            if (properties_file_data.containsKey("application.title")) {
                platform_properties.setProperty("application.title", properties_file_data.getProperty("application.title"));
            } else {
                platform_properties.setProperty("application.title", project_path.split(System.getProperty("file.separator"))[project_path.split(System.getProperty("file.separator")).length - 1].trim());
            }
            if (properties_file_data.containsKey("main.class")) {
                platform_properties.setProperty("main.class", properties_file_data.getProperty("main.class"));
            } else {
                platform_properties.setProperty("main.class", "");
            }

            Properties properties;
            if (new File(deb_properties_path).isFile()) {
                properties = new Properties();
                properties.load(new DataInputStream(new FileInputStream(deb_properties_path)));
                if (!properties.containsKey("application.title")) {
                    if (properties_file_data.containsKey("application.title")) {
                        properties.setProperty("application.title", properties_file_data.getProperty("application.title"));
                    } else {
                        properties.setProperty("application.title", project_path.split(System.getProperty("file.separator"))[project_path.split(System.getProperty("file.separator")).length - 1].trim());
                    }
                }
                if (!properties.containsKey("application.vendor")) {
                    if (properties_file_data.containsKey("application.vendor")) {
                        properties.setProperty("application.vendor", properties_file_data.getProperty("application.vendor"));
                    } else {
                        properties.setProperty("application.vendor", System.getProperty("user.name"));
                    }
                }
                if (!properties.containsKey("deb.output.dir")) {
                    properties.setProperty("deb.output.dir", project_path + System.getProperty("file.separator") + properties_file_data.getProperty("dist.dir") + System.getProperty("file.separator") + "deb");
                }
                if (!properties.containsKey("run.jvmargs")) {
                    if (properties_file_data.containsKey("run.jvmargs")) {
                        properties.setProperty("run.jvmargs", properties_file_data.getProperty("run.jvmargs"));
                    } else {
                        properties.setProperty("run.jvmargs", "");
                    }
                }
                if (!properties.containsKey("main.class")) {
                    if (properties_file_data.containsKey("main.class")) {
                        properties.setProperty("main.class", properties_file_data.getProperty("main.class"));
                    } else {
                        properties.setProperty("main.class", "");
                    }
                }
                if (!properties.containsKey("application.args")) {
                    if (private_properties_file_data.containsKey("application.args")) {
                        properties.setProperty("application.args", private_properties_file_data.getProperty("application.args"));
                    } else {
                        properties.setProperty("application.args", "");
                    }
                }
                if (!properties.containsKey("build.version")) {
                    properties.setProperty("build.version", "0.1-1");
                }
                if (!properties.containsKey("short.desc")) {
                    properties.setProperty("short.desc", properties.getProperty("application.title"));
                }
                if (!properties.containsKey("long.desc")) {
                    if (properties_file_data.containsKey("application.desc")) {
                        properties.setProperty("long.desc", properties_file_data.getProperty("application.desc"));
                    } else {
                        properties.setProperty("long.desc", "");
                    }
                }
                if (!properties.containsKey("application.homepage")) {
                    if (properties_file_data.containsKey("application.homepage")) {
                        properties.setProperty("application.homepage", properties_file_data.getProperty("application.homepage"));
                    } else {
                        properties.setProperty("application.homepage", "");
                    }
                }
                if (!properties.containsKey("application.arch")) {
                    properties.setProperty("application.arch", System.getProperty("os.arch"));
                }
                if (!properties.containsKey("jvm.integration")) {
                    properties.setProperty("jvm.integration", "NONE");
                }
                if (!properties.containsKey("env.vars")) {
                    properties.setProperty("env.vars", "[]");
                }
                if (!properties.containsKey("desktop.file.enable")) {
                    properties.setProperty("desktop.file.enable", "true");
                }
                if (!properties.containsKey("desktop.file.name")) {
                    if (properties_file_data.containsKey("desktop.file.name")) {
                        properties.setProperty("desktop.file.name", properties_file_data.getProperty("desktop.file.name"));
                    } else {
                        properties.setProperty("desktop.file.name", platform_properties.getProperty("application.title"));
                    }
                }
                if (!properties.containsKey("desktop.file.comment")) {
                    if (properties_file_data.containsKey("desktop.file.comment")) {
                        properties.setProperty("desktop.file.comment", properties_file_data.getProperty("desktop.file.comment"));
                    } else {
                        properties.setProperty("desktop.file.comment", platform_properties.getProperty("application.desc"));
                    }
                }
                if (!properties.containsKey("desktop.file.category")) {
                    if (properties_file_data.containsKey("desktop.file.category")) {
                        properties.setProperty("desktop.file.category", properties_file_data.getProperty("desktop.file.category"));
                    } else {
                        properties.setProperty("desktop.file.category", "Utility");
                    }
                }
                if (!properties.containsKey("desktop.file.keywords")) {
                    if (properties_file_data.containsKey("desktop.file.keywords")) {
                        properties.setProperty("desktop.file.keywords", properties_file_data.getProperty("desktop.file.keywords"));
                    } else {
                        properties.setProperty("desktop.file.keywords", platform_properties.getProperty("application.title") + ";Utility");
                    }
                }
                if (!properties.containsKey("desktop.file.icon")) {
                    if (properties_file_data.containsKey("desktop.file.icon")) {
                        properties.setProperty("desktop.file.icon", properties_file_data.getProperty("desktop.file.icon"));
                    } else {
                        properties.setProperty("desktop.file.icon", "");
                    }
                }
                if (!properties.containsKey("desktop.file.run.in.terminal")) {
                    if (properties_file_data.containsKey("desktop.file.run.in.terminal")) {
                        properties.setProperty("desktop.file.run.in.terminal", properties_file_data.getProperty("desktop.file.run.in.terminal"));
                    } else {
                        properties.setProperty("desktop.file.run.in.terminal", "false");
                    }
                }
                if (!properties.containsKey("add.to.global.exec")) {
                    if (properties_file_data.containsKey("add.to.global.exec")) {
                        properties.setProperty("add.to.global.exec", properties_file_data.getProperty("add.to.global.exec"));
                    } else {
                        properties.setProperty("add.to.global.exec", "true");
                    }
                }
            } else {
                File file = new File(deb_properties_path);
                properties = new Properties();
                if (properties_file_data.containsKey("application.title")) {
                    properties.setProperty("application.title", properties_file_data.getProperty("application.title"));
                } else {
                    properties.setProperty("application.title", project_path.split(System.getProperty("file.separator"))[project_path.split(System.getProperty("file.separator")).length - 1].trim());
                }
                if (properties_file_data.containsKey("application.vendor")) {
                    properties.setProperty("application.vendor", properties_file_data.getProperty("application.vendor"));
                } else {
                    properties.setProperty("application.vendor", System.getProperty("user.name"));
                }
                properties.setProperty("deb.output.dir", project_path + System.getProperty("file.separator") + properties_file_data.getProperty("dist.dir"));
                if (properties_file_data.containsKey("run.jvmargs")) {
                    properties.setProperty("run.jvmargs", properties_file_data.getProperty("run.jvmargs"));
                } else {
                    properties.setProperty("run.jvmargs", "");
                }
                if (properties_file_data.containsKey("main.class")) {
                    properties.setProperty("main.class", properties_file_data.getProperty("main.class"));
                } else {
                    properties.setProperty("main.class", "");
                }
                if (private_properties_file_data.containsKey("application.args")) {
                    properties.setProperty("application.args", private_properties_file_data.getProperty("application.args"));
                } else {
                    properties.setProperty("application.args", "");
                }
                properties.setProperty("build.version", "0.0-1");
                properties.setProperty("short.desc", properties.getProperty("application.title"));
                if (properties_file_data.containsKey("application.desc")) {
                    properties.setProperty("long.desc", properties_file_data.getProperty("application.desc"));
                } else {
                    properties.setProperty("long.desc", "");
                }
                if (properties_file_data.containsKey("application.homepage")) {
                    properties.setProperty("application.homepage", properties_file_data.getProperty("application.homepage"));
                } else {
                    properties.setProperty("application.homepage", "");
                }
                properties.setProperty("application.arch", System.getProperty("os.arch"));
                properties.setProperty("jvm.integration", "NONE");
                properties.setProperty("env.vars", "[]");
                properties.store(new FileOutputStream(file), "# Debian package export configuration");
                properties.setProperty("desktop.file.enable", "true");
                if (properties_file_data.containsKey("desktop.file.name")) {
                    properties.setProperty("desktop.file.name", properties_file_data.getProperty("desktop.file.name"));
                } else {
                    properties.setProperty("desktop.file.name", platform_properties.getProperty("application.title"));
                }
                if (properties_file_data.containsKey("desktop.file.comment")) {
                    properties.setProperty("desktop.file.comment", properties_file_data.getProperty("desktop.file.comment"));
                } else {
                    properties.setProperty("desktop.file.comment", platform_properties.getProperty("application.desc"));
                }
                if (properties_file_data.containsKey("desktop.file.category")) {
                    properties.setProperty("desktop.file.category", properties_file_data.getProperty("desktop.file.category"));
                } else {
                    properties.setProperty("desktop.file.category", "Utility");
                }
                if (properties_file_data.containsKey("desktop.file.keywords")) {
                    properties.setProperty("desktop.file.keywords", properties_file_data.getProperty("desktop.file.keywords"));
                } else {
                    properties.setProperty("desktop.file.keywords", platform_properties.getProperty("application.title") + ";Utility");
                }
                if (properties_file_data.containsKey("desktop.file.icon")) {
                    properties.setProperty("desktop.file.icon", properties_file_data.getProperty("desktop.file.icon"));
                } else {
                    properties.setProperty("desktop.file.icon", "");
                }
                if (properties_file_data.containsKey("desktop.file.run.in.terminal")) {
                    properties.setProperty("desktop.file.run.in.terminal", properties_file_data.getProperty("desktop.file.run.in.terminal"));
                } else {
                    properties.setProperty("desktop.file.run.in.terminal", "false");
                }
                if (properties_file_data.containsKey("add.to.global.exec")) {
                    properties.setProperty("add.to.global.exec", properties_file_data.getProperty("add.to.global.exec"));
                } else {
                    properties.setProperty("add.to.global.exec", "true");
                }
            }

            JDialog window = new ExportDebDialog(properties, platform_properties);
            window.setLocationRelativeTo(null);
            window.setVisible(true);
        } catch (Exception exception) {
            Exceptions.printStackTrace(exception);
        }
    }
}
